import os
import sys
import io
import argparse
import hashlib
import re
import shutil
import json
import logging
import dateutil.parser as parser
import calendar
from sqlalchemy.exc import IntegrityError

from pathlib import Path
from dotenv import load_dotenv
load_dotenv()

from curiousphoto.models import Session, Media, Albums
from curiousphoto.exif import get_meta
db = Session()
_dt = None

def import_file(source_fn, target, sub_folder, uuid, ext):
    content_path = None;
    folder =  os.path.join(target, sub_folder)
    Path(folder).mkdir(parents=True, exist_ok=True)
    target_fn = os.path.join(folder, uuid + ext)
    if os.path.isfile(target_fn):
        logging.warning ("duplicate {} ==> {}".format(source_fn,target_fn))
    else:
        shutil.copy(source_fn,target_fn)
    content_path = os.path.join(sub_folder, uuid + '.jpg')
    return content_path



def get_subtarget(taken):
    fold = "{tagen.get('year')}/{taken.get('mon')}"
    return fold


def get_sha256(filepath):
    sha256 = ""
    with open(filepath, mode='rb' ) as f:
        content = f.read()
        size = len(content)
        sha256 = hashlib.sha256(content).hexdigest().zfill(64)
    return size, sha256


def get_camera(exif):
    camera = "{} {}".format(exif["Make"], exif["Model"])
    return camera

def get_taken(dt,source):
    try:
        taken = {}
        tup = dt.timetuple()
        taken['taken_source'] = source
        taken['taken'] = dt
        taken['year'] = tup.tm_year
        taken['month'] = calendar.month_abbr[tup.tm_mon]
        taken['wday'] = calendar.day_abbr[tup.tm_wday]
        taken['mday'] = tup.tm_mday
        taken['yday'] = tup.tm_yday
        taken ['decade'] = (tup.tm_year//10)*10
        return taken
    except Exception as e:
        return {'taken_source':'None'}

def get_location(exif):
    gps = exif.get('GPSInfo')
    if gps is not None:
        location = {}
        location['location_source'] ='exif'
        location['lon'] = gps.get('lon')
        location['lat'] = gps.get('lat')
        location['alt'] = gps.get('lat')
        location['geom'] = 'SRID=4269;POINT(%.8f %.8f)'%(location['lon'], location['lat'])
        return location
    return {'location_source':'None'}

def updatedb(metadata,album_name):
    try:
        media = db.query(Media).filter(Media.uuid==metadata.get('uuid')).first()
        if (not media):
            media = Media(metadata) 
        album = db.query(Albums).filter(Albums.name==album_name).first()
        if (album_name):
            if (not album):
                album = Albums({"name": album_name})
            media.albums.append(album)       
        db.merge(media) #insert or update
        db.commit()
    except Exception as e:
        logging.error(f"Integrity error writing {album_name} {metadata.get('source')} {metadata.get('uuid')} {str(e)}") 
        db.rollback()     
        raise

def import_other(target, root, fname, creator, form, ext):
    try:
        filepath = os.path.join(root,fname)
        size, uuid = get_sha256(filepath)    
        taken=get_taken(_dt,"context") 
        sub_folder = get_subtarget(taken)
        content_path = import_file(filepath, target, sub_folder, uuid, ext)
        metadata = {    
                'uuid': uuid,
                "form": form,
                "content": content_path,
                "size": size,
                "source": fname,
                "camera": "",
                "rank": 0}
        updatedb(metadata, root)
    except Exception as e:
        logging.error(f"Failed to copy {{filepath}}")
        raise

def import_jpeg(target, root, fname, creator, form):
    try:
        filepath = os.path.join(root,fname)
        size, uuid = get_sha256(filepath)
        exif, thumb_image = get_meta(filepath)
        if 'DateTimeDigitized' in exif:
            takenstr = exif['DateTimeDigitized'].replace(":","-",2)
            dt_source = 'exif_dtd'
        elif 'DateTimeOriginal' in exif:
            takenstr = exif['DateTimeOriginal'].replace(":","-",2) 
            dt_source = 'exif_dto'
        else:
            dt_source = 'context'    
        _dt = parser.parse(takenstr)   
        taken=get_taken(_dt,dt_source) 
        sub_folder = get_subtarget(taken)
        location=get_location(exif)
        camera=get_camera(exif)
        content_path = import_file(filepath, target, sub_folder, uuid, '.jpg')
    except  Exception as e:
        logging.error(f"Failed to copy {{filepath}}")
        raise

    metadata = {    'uuid': uuid,
                    "form": form,
                    "content": content_path,
                    "size": size,
                    "source": fname,
                    "camera": camera,
                    "rank": 0,
                    "thumbnail": thumb_image}
    metadata.update(taken)  
    metadata.update(location)  
    updatedb(metadata, root)

      

#TODO files other than jpegs. to facilitate files without metadata it might
# be a good idea to keep original file names/datestamps etc in DB
def import_jpegs(source, target, creator):
    cnt_t = {"files":0, "dirs":0, "jpgs":0, "movs": 0, "errs": 0}
    for root, subdirs, files in os.walk(source): 
        cnt_t["dirs"] += 1
        cnt_f = 0
        cnt_e = 0
        cnt_m = 0
        cnt_j = 0
        for fname in files:
            if (fname in [".DS_Store"]):
                continue
            cnt_f += 1
            if (os.path.splitext(fname)[1].lower()) in ['.jpg', '.jpeg']:
                cnt_j += 1
                try:
                    import_jpeg(target, root, fname,  creator, 'jpg')
                except Exception as e:
                    logging.error(f'error processing {root} {fname}')
                    cnt_e += 1
            else:
                ext = os.path.splitext(fname)[1].lower()
                import_other(target, root, fname,  creator, 'mm', ext)
                cnt_m = +1
        logging.info(f'{root} : {cnt_f} files processed, jpeg={cnt_j}, mov={cnt_m}, err={cnt_e} ')
        cnt_t["files"] += cnt_f
        cnt_t["errs"] += cnt_e
        cnt_t["movs"] += cnt_m
        cnt_t["jpgs"] += cnt_j
    logging.info('processing complete')
    logging.info(cnt_t)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("source")
    parser.add_argument("target")
    parser.add_argument("creator")
    args = parser.parse_args()
    logging.basicConfig(encoding='utf-8', level=logging.INFO)
    salogger = logging.getLogger('sqlalchemy.engine')
    salogger.setLevel(logging.WARNING)    
    import_jpegs(args.source, args.target, args.creator)

if __name__ == '__main__':
    main()