 RC=./dev-env-config/scripts/extrarc
# Note .zlogin runs after /etc/zshrc
# TODO check this runs properly under Windows 10
ZPROFILE=$HOME/.zlogin
BPROFILE=$HOME/.bash_profile 
[ ! $ZPROFILE ] && touch $ZPROFILE
[ ! $BPROFILE ] && touch $BPROFILE
LINE="[ -f $RC ] && source $RC"
if ! grep -qF -- "$LINE" "$ZPROFILE" ; then
  echo "updating zsh profile"
  echo "#DOCS devenv - John Pickerill" >> "$ZPROFILE"
  echo "$LINE" >> "$ZPROFILE"
  [ -f $RC ] && source "$RC"
fi
if ! grep -qF -- "$LINE" "$BPROFILE" ; then
  echo "updating bash profile"
  echo "#DOCS devenv - John Pickerill" >> "$BPROFILE"
  echo "$LINE" >> "$BPROFILE"
  [ -f $RC ] && source "$RC"
fi
