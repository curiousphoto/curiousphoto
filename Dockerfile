# Set the base image to the base image
FROM hmlandregistry/dev_base_python_flask:5-3.6

# Using SQLAlchemy/Postgres?
# See how the required env vars are set here:
# http://git.dev.ctp.local/gadgets/gadget-api/blob/master/Dockerfile

# ----
# Put your app-specific stuff here (extra yum installs etc).
# Any unique environment variables your config.py needs should also be added as ENV entries here
 
ENV ENVIRONMENT_NAME=develop 

# ----

# The command to run the app is inherited from lr_base_python_flask

# Get the python environment ready.
# Have this at the end so if the files change, all the other steps don't need to be rerun. Same reason why _test is
# first. This ensures the container always has just what is in the requirements files as it will rerun this in a
# clean image.


RUN pip3 install -q pip-tools
ADD requirements_test.txt requirements_test.txt
RUN pip3 install -r requirements_test.txt

ADD Makefile Makefile
ADD requirements.in requirements.in 
RUN make depend
#ADD requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

ENV SECRET_KEY='asecretkey' \
APP_NAME='curiousphoto' \
LOG_LEVEL='DEBUG' \
DEBUG='False' \
LOG_DEBUG_FORMAT='False' \
DEFAULT_TIMEOUT="30"

  
#Postgres
ENV SQL_HOST="host.docker.internal:5432" \
SQL_DATABASE="photodb" \
ALEMBIC_SQL_USERNAME="photouser" \
SQL_USE_ALEMBIC_USER="no" \
APP_SQL_USERNAME="photouser" \
SQL_PASSWORD="photopwd" \
SQLALCHEMY_POOL_RECYCLE="3300" \
LOCK_TIMEOUT="15" \
EXPECTED='6,22' \
API_KEY="defaultapikey" \
CONTENT_STORE="/photolib"



CMD ["/src/start.sh"]