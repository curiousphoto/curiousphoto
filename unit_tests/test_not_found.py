import unittest

from curiousphoto.main import app


class TestNotFound(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def test_not_found(self):
        self.assertEqual((self.app.get('/does_not_exist')).status_code, 404)
