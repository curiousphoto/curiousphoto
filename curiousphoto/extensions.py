import requests
import uuid
from flask import request, session, g
from curiousphoto.custom_extensions.enhanced_logging.main import EnhancedLogging
from curiousphoto.protocol import before_request


enhanced_logging = EnhancedLogging()

def register_extensions(app):
    
    """Adds any previously created extension objects into the app, and does any further setup they need."""
    enhanced_logging.init_app(app)

    set_debug(app)

    # enable sessions
    app.secret_key = app.config["APP_SECRET_KEY"]

    # db.init_app(app)
 

    # All done!
    app.logger.info("Extensions registered")


def set_debug(app):
    app.logger.info('set debugger')
    import os
    if app.config['PTVSD']:
        app.logger.info('import debugger')
        import ptvsd
        app.logger.info('waiting debugger attach')
        address = ('0.0.0.0', 8081)
        ptvsd.enable_attach(address)
        ptvsd.wait_for_attach()
        app.logger.info('debugger attached')
    else:
        app.logger.info("Not debug mode")