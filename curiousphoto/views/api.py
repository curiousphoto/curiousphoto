import datetime
import json

from flask import Blueprint, Response,  g, request , jsonify, make_response
from flask import current_app as app
from flask import jsonify,url_for, render_template

import curiousphoto.dependencies.media as media
import os


api = Blueprint('api', __name__)


@api.route("/images", methods=['GET'])
def _explorer():
    pl = media.list(5)
    index = 0;
    for p in pl:
        p['index'] = index
        index += 1
        p['thumbnail'] = f'./api/images/{p.get("uuid")}/thumbnail'
        p['content'] = f'./api/images/{p.get("uuid")}/content'
    return jsonify(pl)

@api.route("/images/<image_id>/metadata",methods=['GET'])
def _get_metadata(image_id):
    data = media.get_meta(image_id)
    return jsonify(data)

@api.route("/images/<image_id>/thumbnail",methods=['GET'])
def _get_thumbnail(image_id):
    content = media.get_thumb(image_id)
    response = make_response(content)
    response.headers['Content-Type'] = 'image/jpeg'
    return response

@api.route("/images/<image_id>/content",methods=['GET'])
def _get_content(image_id):
    data = media.get_meta(image_id)
    
    try:
        filename = os.path.join(app.config["CONTENT_STORE"], data.get("content"))
        app.logger.debug(filename)
        with open(filename, mode='rb' ) as f:
            content = f.read();
            response = make_response(content)
            response.headers['Content-Type'] = 'image/jpeg'
            return response 
    except:
        pass      

@api.route("/dates", methods=['GET']) 
def _get_dates():
    return jsonify({
        '1950':{'query':'decade',
          'parent': True,
          'selected': True,
          'children':{
            '1958':{'query':'year', 'parent':False},
            '1959':{'query':'year', 'parent':False},
          }
        }
    })
      