import datetime
import json

from flask import Blueprint, Response,  g, request , jsonify, make_response
from flask import current_app as app
from flask import jsonify,url_for, render_template

import curiousphoto.dependencies.media as media
import os


explorer = Blueprint('explorer', __name__)

@explorer.route("/explorer/json", methods=['GET'])
def _explorer_json():
    pl = media.list(5)
    return jsonify(pl)
    

@explorer.route("/explorer", methods=['GET'])
def _explorer():
    return render_template("pages/explorer.html",GOOGLE_API_KEY=app.config['GOOGLE_API_KEY'])

