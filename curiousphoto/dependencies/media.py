
from  datetime import datetime

from sqlalchemy.orm.exc import NoResultFound
from curiousphoto.models import Media
from curiousphoto.models.database  import Session, engine, Base

from flask import current_app as app

db = Session()
# Base.metadata.create_all(bind=engine)

def list(limit = 100):
    records = []
    retval = []
    try:
      records = (db.query(Media)
                  .order_by(Media.uuid)
                  .limit(limit)
                  .all()
                  )

    except Exception as e:
      
      pass 
    finally:
      db.rollback()
    for r in records:
        retval.append(r.serialize())
    return retval;

def get_thumb(id):
  try:
    result = db.query(Media).get(id).thumb()
  finally:
    # db.rollback()
    pass
  return result

def get_meta(id):
  try:
    result = db.query(Media).get(id).meta()
  finally:
    # db.rollback()
    pass
  return result