# Import every blueprint file
from curiousphoto.views import general , explorer, api


def register_blueprints(app):
    """Adds all blueprint objects into the app."""
    app.register_blueprint(general.general)
    app.register_blueprint(explorer.explorer) 
    app.register_blueprint(api.api,
        url_prefix='/api') 

    # All done!
    app.logger.info("Blueprints registered")
