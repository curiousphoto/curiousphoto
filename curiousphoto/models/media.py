import json
import sys
from datetime import datetime

from sqlalchemy.sql import func
from sqlalchemy.dialects.postgresql import JSONB

from sqlalchemy import create_engine  
from sqlalchemy import Table, Column, ForeignKey
from sqlalchemy import String, DateTime, LargeBinary, Float, Integer
from geoalchemy2 import Geometry
from sqlalchemy.orm import relationship
from curiousphoto.models.database import Base, cfg

# need to have method of 
# - creating albums (and maybe sub-albums)
# - identifying similar photos and the lead Photo
# - having a story for a group of photos 

albums_media_link = Table('albums_media', Base.metadata,
    Column('album', ForeignKey('albums.id')),
    Column('uuid', ForeignKey('media.uuid'))
)
class Albums(Base):
    __tablename__ = "albums"
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, index=True, unique=True)
    story = Column(String)
    media = relationship(
        "Media",
        secondary=albums_media_link,
        back_populates="albums")   

    def __init__(self, data_dictionary):
        for key, value in data_dictionary.items():
            setattr(self, key, value)
    def serialize(self):        
        return {
            "name": self.name,
            "story": self.story
        }


class Media(Base):
    __tablename__ = "media"
    uuid = Column(String, primary_key=True) # sha256 of media content
    form = Column(String, ) # data type e.g. jpg etc
    content = Column(String) # url/path of media content
    size = Column(Integer) # size in bytes of media content
    lead = Column(String) # this is very similar to this one
    creator = Column(String) # the photographer
    title = Column(String) # short description 
    description = Column(String) # long description
    source = Column(String, index=True) # this indicates the origin  
    camera = Column(String, index=True) # make and model 
    rank = Column(String, index=True) # indication of interestingness

    taken = Column(DateTime(), index=True)
    taken_source = Column(String) # manual | exif
    decade = Column(Integer, index=True)
    year = Column(Integer, index=True)
    month = Column(String, index=True)
    yday = Column(Integer, index=True)   
    mday = Column(Integer, index=True)
    wday = Column(String, index=True)
 
    location_source = Column(String) # Manual | exif
    geom = Column(Geometry(geometry_type='POINT', srid='4269'))
    lat = Column(Float)  # latitude
    lon = Column(Float) # longtitude
    alt = Column(Float) # altitude

    thumbnail = Column(LargeBinary)

    albums = relationship(
        "Albums",
        secondary=albums_media_link,
        back_populates="media")

    def __init__(self, data_dictionary):
        self.update(data_dictionary)

    def update(self, data_dictionary):
        for key, value in data_dictionary.items():
            setattr(self, key, value)

    def __repr__(self):
        """Returns string representation of an employee object"""
        return json.dumps(self.serialize())

    def thumb(self):
        return self.thumbnail
    
    def meta(self):
        return {
            "uuid":  self.uuid,
            "form": self.form,
            "content": self.content,
            "size": self.size,
            "creator": self.creator,
            "title": self.title,
             "description": self.description,
             "source": self.source,
            "camera": self.camera,
            "rank": self.rank,

            "taken_source": self.taken_source,
            "taken": self.taken.isoformat(),
            "decade": self.decade,
            "year": self.year,
            "month": self.month,
            "yday": self.yday,
            "mday": self.mday,
            "wday": self.wday,

            "geom_source": self.geom_source,
            "lon": self.lon,
            "lat": self.lat,
            "alt": self.alt
        }


    def serialize(self):        
        return {
                "uuid": self.uuid,
                "form": self.form,
                "taken":self.taken.isoformat(), 
                "lon":self.lon,
                "lat":self.lat
            }