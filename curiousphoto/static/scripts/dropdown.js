function get_node(node,filters){
  let url = node 
  let query = ''
  for (const filter of filters) {
    query = (query.length)?query+'&':query+'?'
    query = query + filter.name + '=' + filter.value
  }
  url = url + query;
  const       tree={ "decades": { 
    "name":"decade", 
    "values":["1950s","1960s"],
    }, 
    "years?decade=1950s": {
      "name":"year", 
      "values":["1958","1959"],
    },
    "months?decade=1950s&year=1958": {
      "name":"month", 
      "values":["may","july"],
    }
  };
  if (url in tree)
    return tree[url]
  else
    return {"values":[]}
}

Vue.component('ddtree',{
  // props:{
  //   tree:{
  //     type: Object,
  //     required: true
  //   }
  // },
  data() {
    return {
      filter: '',
      nodes: {
        "decades":{"child":"years", "callback":get_node},
        "years":{"child":"months", "callback":get_node},
        "months":{"callback":get_node}
      }

    }
  },
  methods: {
    onFilter(filter) {
      this.filter = filter;
      this.$emit('clear_images',filter)
    },
    getImages(){
      this.$emit('getImages');
    }

  },
  emits:['onfilter', 'getImages'],
  template:
`
<div class="display_flex">
  <dropdown name="decades" 
    :nodes="nodes" 
    @onfilter="onFilter"></dropdown>
  <button @click="getImages">getImages</button>
</div>
`

})

Vue.component('dropdown',{
  props: {
    name: {
      type: String,
      required: true,
      default: 'dropdown',
      note: 'Input name'
    },
    filters: {
      type: Array,
      required: false,
      default(){ return []},
      note: 'Search filters'
    },
    nodes:{
      type: Object,
      required: false,
      default(){ return []},
      note: 'Search path'      
    },
    pre: {
      type: String,
      required: false,
      default: '',
      note: 'string before input'
    },
    disabled: {
      type: Boolean,
      required: false,
      default: false,
      note: 'Disable the dropdown'
    },
    maxItem: {
      type: Number,
      required: false,
      default: 6,
      note: 'Max items showing'
    }
  },
  data() {
    return {
      selected: '',
      optionsShown: false,
      optionFilter: '',
      node: {},
      childFilters:[],
      placeholder: 'loading ...'
    }
  },
  computed: {
    options(){
      this.selected = '';
      this.optionFilter = '';
      this.node = this.nodes[this.name].callback(this.name,this.filters);
      if (!this.node.values.length)
        this.placeholder = "no " + this.name + " available";
      else
        this.placeholder = "select " + this.name
      return this.node.values
    },


    filteredOptions() {
      const filtered = [];
      const regOption = new RegExp(`^${this.optionFilter}`, 'ig');
      for (const option of this.options) {
        if (this.optionFilter.length < 1 || option.match(regOption)){
          if (filtered.length < this.maxItem) filtered.push(option);
        }
      }
      return filtered;
    },
    input_style(){
      let width;
      if (this.selected)
        width = this.selected.length+1;
      else 
        width = this.placeholder.length+1;
      return `max-width:${width}ch;min-width:${width}ch`;
    },
    child(){
      if (this.selected && this.nodes[this.name] && this.nodes[this.name].child)
        return this.nodes[this.name].child;
      else
        return null;  
    }
  },
  emits: ['onfilter'],
  methods: {
    onFilter(filters) {
      this.$emit('onfilter',filters)
    },
    selectOption(option) {
      this.selected = option;
      this.optionsShown = false;
      this.optionFilter = this.selected;
      const filter={"name":this.node.name ,"value":this.selected};
      this.childFilters = [...this.filters,filter];
      this.onFilter(this.childFilters)      
    },
    showOptions(){
      if (!this.disabled) {
        this.optionFilter = '';
        // this.selected='';
        this.optionsShown = true;
      }
    },
    exit() {
      if (!this.selected) {
        this.selected = '';
      } else {
        this.optionFilter = this.selected;
      }
      this.optionsShown = false;
    },
    // Selecting when pressing Enter
    keyMonitor: function(event) {
      if (event.key === "Enter" && this.filteredOptions[0])
        this.selectOption(this.filteredOptions[0]);
      else
        this.optionsShown=true;
    }
  },
  watch: {
    optionFilter() {
      if (this.filteredOptions.length === 0) {
        this.selected = '';
        if (this.optionFilter.length > 0 )
          this.optionFilter=this.optionFilter.substring(0,this.optionFilter.length-1);
      }
    }
  },
  template:
  `
  <div class="dropdown" v-if="options">
    <div :class="'dropdown-node ' + pre">
      <!-- Dropdown Input -->
      <input 
        class="dropdown-input"
        v-bind:class="{selected:selected}"
        :style = "input_style"
        :name="name"
        @focus="showOptions()"
        @blur="exit()"
        @keyup="keyMonitor"
        v-model="optionFilter"
        :disabled="disabled"
        :placeholder="placeholder" />

      <!-- Dropdown Menu -->
      <div class="dropdown-content"
        v-show="optionsShown">
        <div
          class="dropdown-item"
          @mousedown="selectOption(option)"
          v-for="(option,index) in filteredOptions"
          :key="index">
            {{ option || '-' }}
        </div>
      </div>
    </div>

    <dropdown v-if="child" :name="child" 
      :filters="childFilters"
      :nodes="nodes"
      class="pre" 
      @onfilter="onFilter"></dropdown>
  </div>
  `


})