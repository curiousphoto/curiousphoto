


Vue.component('search',{
  data() {
    return {
      loading:false,
      query_display:false,
      }
  },
  emits: ['new_images','loading','clear_images'],
  methods: {
    async getImageList(){
      this.loading=true;
      const res = await fetch('./api/images');
      const data = await res.json();
      this.$emit('new_images',data);
      this.loading=false;
    },
    clear_images(){
      this.$emit('clear_images')
    }
  },
  template:
  `
  <div>
    <div class="tabs_wrapper">
      <tabs>
        <tab title="Period">
          <ddtree @getImages="getImageList" @clear_images="clear_images"></ddtree>
        </tab>
        <tab title="Date Range">
          <grid @getImages="getImageList" @clear_images="clear_images"></grid>
        </tab>
        <tab title="Trip">Hello From Tab 3</tab>
        <tab title="Story">Hello From Tab 4</tab>
        <tab title="Album">Hello From Tab 4</tab>
        <tab title="Location">Hello From Tab 4</tab>
        <tab title="People">Hello From Tab 4</tab>
      </tabs>
    </div>
    <p v-if="loading">loading...</p>

  </div>
  `
})

Vue.component('tabs',{
  props: {
    mode: {
      type: String,
      default: 'light'
    }
  },
  data () {
    return {
      selectedIndex: 0, // the index of the selected tab,
      tabs: [],        // all of the tabs
      toggle_header: true
    }
  },
  created () {
    this.tabs = this.$children
  },
  mounted () {
    this.selectTab(0)
  },
  methods: {
      selectTab (i) {
        this.selectedIndex = i
        
        // loop over all the tabs
        this.tabs.forEach((tab, index) => {
          tab.isActive = (index === i)
        })
      },
      toggle() {
        this.toggle_header =!this.toggle_header;
      }
  },
  template:
  `
  <div class="display_flex">
    <button class="tab-toggle" @click="toggle" >\></button>
    <div :class='{"tabs__light": mode === "light", 
                  "tabs__dark": mode === "dark", 
                  "display_flex": !toggle_header}'>
      <ul class='tabs__header'>
        <li v-for='(tab, index) in tabs' 
          :key='tab.title' 
          @click='selectTab(index)'
          v-show='toggle_header' 
          :class='{"tab__selected": (index == selectedIndex)}'>
          {{ tab.title }} 
        </li>
      </ul>
      <slot></slot>
    </div>
  </div>
  `
})


Vue.component ('tab',{
  props: {
    title: {
      type: String,
      default: 'Tab'
    }
  },
  data () {
    return {
      isActive: true
    }
  },

  template:
  `
  <div class='tab' v-show='isActive'>
    <slot></slot>
  </div>
  `  

})






