Vue.component('thumbnails',{
  props:['images','selected','thumbs'],
  methods: {
    select_image(index){
      this.$emit('select_image', index);
    }
  },
  emits: ["select_image"],
  template:`
  <div v-if="images" class="thumbnails">
    <ul>
      
      <li v-for="n in thumbs" :key="n">
            <thumbnail :image=images[n] :selected=selected
              @select_image="select_image($event)"></thumbnail>
      </li>
    </ul>
  </div>
  `
})

Vue.component('thumbnail', {
  props: ['image','selected'],
  methods:{
    select_image(index){
      this.$emit('select_image',index);
     }
  },
  computed:{
    isSelected(){
      return (this.image.index==this.selected);
    }
  },
  emits: ['select_image'],
  template: 
    `<button class="thumbnail" :class="{selected:isSelected}" v-on:click="select_image(image.index)"> 
      <img :src="image.thumbnail" alt="image.uuid"></img> 
    </button>  
    `
})  