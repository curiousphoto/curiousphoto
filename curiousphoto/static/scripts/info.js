

compMeta = Vue.component('info',{
  props:['image'],
  template:
    `
    <div class="info" >
      <p>EXIF Data</p>
      <table>
        <tr><td>uuid</td><td>{{image.uuid}}</td></tr>     
        <tr><td>taken</td><td>{{image.taken}}</td></tr>     
        <tr><td>lat</td><td>{{image.lat}}</td></tr>      
        <tr><td>lon</td><td>{{image.lon}}</td></tr>       
      </table>
    </div>
    `
})