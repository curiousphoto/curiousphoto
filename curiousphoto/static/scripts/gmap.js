
function getLoader(){
  return new google.maps.plugins.loader.Loader({
    apiKey: "AIzaSyBDeXvfqXU7785XXIb-w_6kdNqIvdtt88U",
    version: "weekly",
    libraries: []
  });
}


function get_location(image) {
  if (typeof image == 'undefined'){
    return null;
  }
  if (image.hasOwnProperty("lat") && image.lat){
    return {lat: image.lat, lng: image.lon};
  } else {
    return null;
  }
}

function mapOptions(location){
  return {
    center: location,
    zoom: 10
  };

}



gmapLoader = Vue.component('gmaploader',{
  props:['location'],
  data() {
    return {
      google: null,
      map: null
    }
  },
  async mounted() {
    const googleMapApi = await getLoader().load();
    this.google = googleMapApi
    this.initializeMap()
  },
  methods: {
    initializeMap() {
      const mapContainer = this.$refs.gmap
      this.map = new this.google.maps.Map(
        mapContainer, mapOptions(this.location)  
      )
    }
  },
  template:
    `
    <div class="gmaploader">
      <div class="map" ref="gmap"></div>
      <template v-if="Boolean(this.google) && Boolean(this.map)">
        <slot
          :google="google"
          :map="map"
        />
      </template>  
     </div> 
    ` 
})

gmap = Vue.component('gmap',{
  props:['image'],
  data() {
    return {
      marker: null,
    }
  },
  computed:{
    location(){
      return get_location(this.image);
    }

  },
  template:
    `
    <div class="gmap">
      <gmaploader v-if="location" :image="image" :location=location>
        <template slot-scope="{ google, map }">
          <map-marker :google='google' :map='map' :location='location'></map-marker>
        </template>
      </gmaploader>
      <p v-if="location">{{location.lat}} {{location.lng}}</p>
    </div>
  `
 
})

marker = Vue.component('map-marker',{
  props: {
    google: {
      type: Object,
      required: true
    },
    map: {
      type: Object,
      required: true
    },
    location: {
      type: Object,
      required: true
    }},
  data() {
    return {
      marker: null

    }
  },  
  watch: {
    location(newloc,oldloc) {
      if (this.map){
        this.map.setCenter(newloc);
        this.marker.setMap(null);
        this.marker = new this.google.maps.Marker({
            position: newloc,
            map: this.map,
        });
      } 
    }
  }, 
  mounted() {
    if (this.map){
      this.map.setCenter(this.location);
      this.marker = new this.google.maps.Marker({
          position: this.location,
          map: this.map,
      });
    }    
  },
  template:
  `
    <div class="marker">
    </div>
  `

})