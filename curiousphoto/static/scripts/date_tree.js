
treeComp = Vue.component ('date-tree',{
  props: ['date_list'],
  methods: {
    make_query(value,name,queries){
      const query = {"value": value, "name": name}
      if (!queries) {queries = [query];}
      else { queries.push(query);} 
      this.$emit('query', queries)
    }
  },  
  emits: ['query'],
  template:
  `
  <div>
    <p>date-tree</p>
    <ul class="date-tree">
      <li class="tree-item" v-for="(value, name) in date_list" @click.stop="make_query(value.query,name) ">
          {{name}}
          <date-tree :date_list="value.children" @query="make_query(value.query,name,$event)"></date-tree>
      </li>
    </ul>
  </div>
  `

})

itemComp = Vue.component('tree-item',{
  props: ['parents'],
  template:
  `
  <div v-for="(pvalue, pname) in parents" :key=pname>
    <p class="pname"> {{pvalue}} </p>
  </div>
  `



})