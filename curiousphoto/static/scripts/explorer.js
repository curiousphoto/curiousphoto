const RootComponent = {
  el: '#explorer',

}

Vue.component('explorer',{
  data() {
    return {
      images: null,
      selected: 0,
      thumbs : [0,1],
      thumbs_max: 5,
      thumbs_len: 5,
      thumbs_mid: 2,
      max_start: 3
    }
  },
  methods: {
    new_images(images) {
      this.images=images;
      if (this.images.length < this.thumbs_max) {
        this.thumbs_len = this.images.length;
      } else {
        this.thumbs_len = this.thumbs_max;
      }
      this.thumbs_mid = Math.floor(this.thumbs_len/2);
      this.max_start = images.length - this.thumbs_len;
      this.select_image(0);
    },
    select_image(selected) {
      this.selected=selected;
      var start = this.selected - this.thumbs_mid;
      if (start < 0) {
        start = 0;
      }
      if (start > (this.max_start)) {
        start = this.max_start;
      }

      this.thumbs = Array(this.thumbs_len).fill().map((_, index) => start + index);
    },
    clear_images(){
      this.images=null;
    },
    next_image(){
      if ((this.images.length - this.selected) > 1) {
        this.select_image(this.selected +1)
      } 
    },
    prev_image(){
      if (this.selected > 0)  {    
        this.select_image(this.selected -1)
      }
    }
  },
  computed:{
    selected_image(){
      if (this.images && (this.images.length>this.selected)) {
        return this.images[this.selected];
      }
      else {
        return null;
      }
    }
  },
  template:
  `
  <div class="explorer">
    <search @new_images="new_images" 
            @clear_images="clear_images"></search>
    <thumbnails :images="images" :selected="selected" :thumbs="thumbs"
            @select_image="select_image($event)"></thumbnails>
    <photo :image="selected_image" 
            @next_image="next_image"
            @prev_image="prev_image"></photo>
  </div>
  `
})
 




compPhoto = Vue.component('photo',{
  props: ['image'],
  methods: {
    next_image(){this.$emit('next_image');},
    prev_image(){this.$emit('prev_image')}
  },
  emits:['next_image','prev_image'],
  template:
  `
  <div class="photo" v-if="image">
    <media :image="image" 
      @next_image="next_image"
      @prev_image="prev_image"></media>
    <meta-frame  :image="image"></meta-frame>
  </div>
  `

})

compMeta = Vue.component('meta-frame',{
  props: ['image'],
  template:
  `
  <div class="meta-frame">
    <p>META FRAME</p>
    <info  :image="image"></info>
    <gmap  :image="image"></gmap>
  </div>
  `
})



compMedia = Vue.component('media', {
  props: ['image'],
  methods: {
    next_image(){this.$emit('next_image');},
    prev_image(){this.$emit('prev_image')}
  },
  emits:['next_image','prev_image'],
  template: 
  `
    <div class="media">
        <img v-if="Boolean(image.uuid)" class="content" :src="image.content" alt="image.uuid">
          <button class="ol-button right" @click="next_image">\></button>
          <button class="ol-button left" @click="prev_image">\<</button>      
        </img>
    </div>  
    `
})



var app = new Vue(RootComponent)


