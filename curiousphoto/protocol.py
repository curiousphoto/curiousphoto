import requests
import uuid
from flask import request, current_app, session, g, abort
 

 

class RequestsSessionTimeout(requests.Session):
    """Custom requests session class to set some defaults on g.requests"""
    def request(self, *args, **kwargs):
        # Set a default timeout for the request.
        # Can be overridden in the same way that you would normally set a timeout
        # i.e. g.requests.get(timeout=5)
        if not kwargs.get('timeout'):
            kwargs['timeout'] = current_app.config['DEFAULT_TIMEOUT']

        return super(RequestsSessionTimeout, self).request(*args, **kwargs)


def before_request():
    # Sets the transaction trace id on the global object if provided in the HTTP header from the caller.
    # Generate a new one if it has not. We will use this in log messages.
    g.trace_id = request.headers.get('X-Trace-ID', uuid.uuid4().hex)
    # We also create a session-level requests object for the app to use with the header pre-set, so other APIs
    # will receive it. These lines can be removed if the app will not make requests to other LR APIs!
    g.requests = RequestsSessionTimeout()


 
    session.permanent = True  # chrome cookie expiry at end of session is unpredictable so use configured timeout
    current_app.logger.debug(request.environ['PATH_INFO'])
    if request.method not in ['OPTIONS'] and '/health' not in request.environ['PATH_INFO']:
        api_key = request.headers.get('X-Api-Key', None)
        if api_key != current_app.config.get('API_KEY', None):
            current_app.logger.warn("invalid api key received")
            abort(403, 'Invalid API Key {0}'.format(request.data))

