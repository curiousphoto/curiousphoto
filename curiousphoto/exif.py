import datetime
from fractions import Fraction


from PIL import Image
import PIL.ExifTags
import os
import io

MAX_SIZE = 128, 128

lookups = {
    "MeteringMode": { 
        0: "Undefined",
        1:  "Average",
        2:  "Center-weighted average",
        3:  "Spot",
        4:  "Multi-spot",
        5:  "Multi-segment",
        6:  "Partial"},
    "ExposureProgram": {
        0:  "Undefined",
        1:    "Manual",
        2:    "Program AE",
        3:    "Aperture-priority AE",
        4:    "Shutter speed priority AE",
        5:    "Creative (Slow speed)",
        6:    "Action (High speed)",
        7:    "Portrait ",
        8:    "Landscape",
        9:    "Bulb"},
    "ResolutionUnit": {
        0:   "",
        1:   "Undefined",
        2:   "Inches",
        3:   "Centimetres"},
    "Orientation": {
        0:  "",
        1:   "Horizontal",
        2:    "Mirror horizontal",
        3:    "Rotate 180",
        4:    "Mirror vertical",
        5:    "Mirror horizontal and rotate 270 CW",
        6:    "Rotate 90 CW",
        7:    "Mirror horizontal and rotate 90 CW",
        8:    "Rotate 270 CW"},
    "Flash": {
        0x0: "No Flash",
        0x1: "Fired",
        0x5: "Fired, Return not detected",
        0x7: "Fired, Return detected",
        0x8: "On, Did not fire",
        0x9: "On, Fired",
        0xd: "On, Return not detected",
        0xf: "On, Return detected",
        0x10: "Off, Did not fire",
        0x14: "Off, Did not fire, Return not detected",
        0x18: "Auto, Did not fire",
        0x19: "Auto, Fired",
        0x1d: "Auto, Fired, Return not detected",
        0x1f: "Auto, Fired, Return detected",
        0x20: "No flash function",
        0x30: "Off, No flash function",
        0x41: "Fired, Red-eye reduction",
        0x45: "Fired, Red-eye reduction, Return not detected",
        0x47: "Fired, Red-eye reduction, Return detected",
        0x49: "On, Red-eye reduction",
        0x4d: "On, Red-eye reduction, Return not detected",
        0x4f: "On, Red-eye reduction, Return detected",
        0x50: "Off, Red-eye reduction",
        0x58: "Auto, Did not fire, Red-eye reduction",
        0x59: "Auto, Fired, Red-eye reduction",
        0x5d: "Auto, Fired, Red-eye reduction, Return not detected",
        0x5f: "Auto, Fired, Red-eye reduction, Return detected  "   },             
    "FileSource": {
        1: "Film Scanner",
        2: "Reflection Print Scanner",
        3: "Digital Camera"}
    }




def get_meta(filepath):
    exif = {}
    thumb = None
    try:
        with PIL.Image.open(filepath) as img:
            exif = get_exif(img)
            img.thumbnail(MAX_SIZE)
            thumb = image_to_bytes(img) 
    except Exception as e:    
        raise ExifError(filepath)
    return exif, thumb

def image_to_bytes(image:Image):
  imgByteArr = io.BytesIO()
  image.save(imgByteArr, format=image.format)
  imgByteArr = imgByteArr.getvalue()
  return imgByteArr


RATIONALS = [
    "FNumber",
    "FocalLength",
    "XResolution",
    "YResolution",
    "ExposureTime",
    "ExposureBiasValue",
    'CompressedBitsPerPixel']
HUMANS = [
    'MaxApertureValue',
    'ExifImageWidth',
    'FocalLengthIn35mmFilm',
    'ExifImageHeight',
    'Model',
    'Make',
    'Software',
    'DigitalZoomRatio',
    'ISOSpeedRatings']
DATETIMES = [
    "DateTime",
    "DateTimeOriginal",
    "DateTimeDigitized"]
LOOKUPS = [ 
    "FileSource",
    "Flash",
    "Orientation",
    "ResolutionUnit",
    "ExposureProgram",
    "MeteringMode"]

def get_exif(img):
    exif = {} 

    exif_data_raw = img._getexif()
    for tag, value in exif_data_raw.items():
        decoded_tag = PIL.ExifTags.TAGS.get(tag, tag) 
        if decoded_tag in RATIONALS:
            value = _derationalize(value)
            exif[decoded_tag] = value
        elif decoded_tag in HUMANS:
            exif[decoded_tag] = str(value)
        elif decoded_tag in DATETIMES:
            exif[decoded_tag] = value
        elif decoded_tag in LOOKUPS:
            value = lookups.get(decoded_tag,{}).get(value,'unknown')
            exif[decoded_tag] = value;
        elif decoded_tag == "GPSInfo":
            data = {}
            for t in value:
                sub_decoded = PIL.ExifTags.GPSTAGS.get(t,t)
                data[sub_decoded] = value[t]
            exif[decoded_tag] = get_coordinates(data)                  
        else:
            exif[decoded_tag] = str(value)

    return exif 

def generate_exif_dict(image):

    """
    Generate a dictionary of dictionaries.
    The outer dictionary keys are the names
    of individual items, eg Make, Model etc.
    The outer dictionary values are themselves
    dictionaries with the following keys:
        tag: the numeric code for the item names
        raw: the data as stored in the image, often
        in a non-human-readable format
        processed: the raw data if it is human-readable,
        or a processed version if not.
    """

    try:

        exif_data_PIL = image._getexif()

        exif_data = {}

        for k, v in PIL.ExifTags.TAGS.items():

            if v == "GPSInfo":
                pass

            if k in exif_data_PIL:
                value = exif_data_PIL[k]
            else:
                value = None

            if len(str(value)) > 64:
                value = str(value)[:65] + "..."

            exif_data[v] = {"tag": k,
                            "raw": value,
                            "processed": value}

        exif_data = _process_exif_dict(exif_data)

        return exif_data

    except IOError as ioe:

        raise       


def get_decimal_from_dms(dms, ref):
    if ((dms == None) | (ref == None)):
        return None 

    degrees = dms[0]
    minutes = dms[1]/ 60.0
    seconds = dms[2]/ 3600.0

    if ref in ['S', 'W']:
        degrees = -degrees
        minutes = -minutes
        seconds = -seconds

    return round(degrees + minutes + seconds, 5)


def get_coordinates(geotags):
    lat = get_decimal_from_dms(geotags.get('GPSLatitude'), geotags.get('GPSLatitudeRef'))
    lon = get_decimal_from_dms(geotags.get('GPSLongitude'), geotags.get('GPSLongitudeRef'))
    alt = _derationalize(geotags.get('GPSAltitude'))
    direction = _derationalize(geotags.get('GPSImgDirection'))
    return {'lat' : lat, 'lon': lon, 'lat': alt, 'direction': direction}


def _derationalize(rational):
    if (hasattr(rational, "numerator") & hasattr(rational, "denominator")):
       return rational.numerator / rational.denominator
    else:
        return None




