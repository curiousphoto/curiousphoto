#!/usr/bin/env python3

import os
 
from flask_script import Manager
from curiousphoto.main import app

# ***** For Alembic start ******
# from flask_migrate import Migrate, MigrateCommand
# from curiousphoto.models import *    # noqa
# from curiousphoto.extensions import db

# from flask_migrate import Migrate
# ***** For Alembic end *****

manager = Manager(app)

# ***** For Alembic start ******
manager.add_command('db', MigrateCommand)
# ***** For Alembic end ******

@manager.command
def runserver(port=9998, host='localhost'):
    """Run the app using flask server"""

    os.environ["PYTHONUNBUFFERED"] = "yes"
    os.environ["LOG_LEVEL"] = "DEBUG"
    os.environ["COMMIT"] = "LOCAL"

    PTVSD = (os.getenv('PTVSD', 'False')=="True")
    app.run(debug=not PTVSD, use_reloader=not PTVSD, port=int(port), host=host)


if __name__ == "__main__":
    manager.run()
